import React from 'react'
import './App.css'
import { BrowserRouter as Router, Route, Link, Routes } from 'react-router-dom'
import AutomaticBatching from './automatic-batching'
import { AppBar, Typography, Toolbar, Link as MuiLink, GlobalStyles, CssBaseline } from '@mui/material'
import V18 from './version18'

function App() {
  return (
    <Router>
      <GlobalStyles styles={{ ul: { margin: 0, padding: 0, listStyle: 'none' } }} />
      <CssBaseline />
      <AppBar position='static' color='default' elevation={0} sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}>
        <Toolbar sx={{ flexWrap: 'wrap' }}>
          <Typography variant='h6' color='inherit' noWrap sx={{ flexGrow: 1 }}>
            React v18
          </Typography>

          <MuiLink variant='button' color='text.primary' href='#' sx={{ my: 1, mx: 1.5 }}>
            <Link to='/'>Version 18</Link>
          </MuiLink>

          <MuiLink variant='button' color='text.primary' href='#' sx={{ my: 1, mx: 1.5 }}>
            <Link to='/automatic-batching'>Automatic Batching</Link>
          </MuiLink>

          <MuiLink variant='button' color='text.primary' href='#' sx={{ my: 1, mx: 1.5 }}>
            <Link to='/transitions'>Transitions</Link>
          </MuiLink>

          <MuiLink variant='button' color='text.primary' href='#' sx={{ my: 1, mx: 1.5 }}>
            <Link to='/suspense'>Suspense</Link>
          </MuiLink>

          <MuiLink variant='button' color='text.primary' href='#' sx={{ my: 1, mx: 1.5 }}>
            <Link to='/client-server-rendering-apis'>Client Server Rendering APIs</Link>
          </MuiLink>

          <MuiLink variant='button' color='text.primary' href='#' sx={{ my: 1, mx: 1.5 }}>
            <Link to='/strict-mode'>Strict Mode</Link>
          </MuiLink>

          <MuiLink variant='button' color='text.primary' href='#' sx={{ my: 1, mx: 1.5 }}>
            <Link to='/hooks'>Hooks</Link>
          </MuiLink>
        </Toolbar>
      </AppBar>
      <Routes>
        <Route exact path='/' element={<V18 />} />
        <Route path='/automatic-batching' element={<AutomaticBatching />} />
        <Route path='/transitions' element={<AutomaticBatching />} />
        <Route path='/suspense' element={<AutomaticBatching />} />
        <Route path='/client-server-rendering-apis' element={<AutomaticBatching />} />
        <Route path='/strict-mode' element={<AutomaticBatching />} />
        <Route path='/hooks' element={<AutomaticBatching />} />
      </Routes>
    </Router>
  )
}

export default App
